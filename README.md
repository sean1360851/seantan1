### Hi there 👋

🔭 I’m currently working on creating an NFT marketplace for [Auxiun](http://auxiun.com/), check out my repos related to Auxiun.  
🤖 Also building lots of crypto bots for clients and myself which I will never make public.  
📫 How to reach me: seantan374@gmail.com  
🤣 Fun fact: Looking at code is something I most likely do first thing in the morning and the last thing before I sleep  

Feel free to connect with me on [Linkedin](https://www.linkedin.com/in/sean-tan1/) or check out my [website](https://seantan1.github.io/)  
  
You can also visit my [University GitHub account](https://github.com/sean-rmit) if you're interested to look at more projects I've done in university.
